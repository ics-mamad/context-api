import {createContext,useState} from 'react'

export const CounterContext = createContext(null)

export const ProviderContext = (props) =>{

    // eslint-disable-next-line
    const [count,setCount] = useState(0)
    

    return <CounterContext.Provider value={{count,setCount, name:"Alyani"}}>
        { props.children }
    </CounterContext.Provider>
}