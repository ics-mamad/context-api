import {useContext} from 'react'
import Counter from './components/Counter';
import { CounterContext } from './contexts/CounterContext';


function App() {
  
  const cc = useContext(CounterContext)
  console.log("Name is : ",cc.name)
  


  return (
    <div>
     <h1>Counter is : {cc.count}</h1>
     <Counter />
    </div>
  );
}

export default App;
