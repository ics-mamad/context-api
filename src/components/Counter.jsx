import React from 'react'
import {useContext} from 'react'
import { CounterContext } from '../contexts/CounterContext'

const Counter = () => {

    const cc = useContext(CounterContext)

  return (
    <div>
      <button onClick={()=>{
        cc.setCount(cc.count + 1)
      }}>+</button>


      <button onClick={()=>{
        cc.setCount(cc.count - 1)
      }}>-</button>
    </div>
  )
}

export default Counter
